# -*- coding: UTF-8 -*-
from pymongo import MongoClient

host='localhost'
port=27017
admin_db='test'
db_name='test'
pwd='test123'

def getMyMongo():
    mongo_client = MongoClient(host=host, port=port)
    admin = mongo_client[admin_db]
    admin.authenticate(name=db_name, password=pwd)
    return mongo_client
