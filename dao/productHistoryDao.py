# -*- coding: UTF-8 -*-


# 导入domain模块
import domain

# myMongo模块
from conf import myMongo

# 获取Mongo的client
mongo_client = myMongo.getMyMongo()

# 切换到test数据库,如果没有就自动创建
test_db = mongo_client.test

# 使用product集合，如果没有就自动创建
coll_product_history = test_db.product_history


# save
def save(price_history):
    # 向集合中插入数据
    coll_product_history.insert({'product_id': price_history.product_id, 'name': price_history.name,
                                 'tm_price_record': price_history.tm_price_record,
                                 'jd_price_record': price_history.jd_price_record})


# find

def find_by_name(name):
    coll_product_history.find_one({'name': name})


def find_by_product_id(id):
    coll_product_history.find_one({'product_id': id})
