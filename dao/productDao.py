# -*- coding: UTF-8 -*-

# 导入domain模块
import domain

# myMongo模块
from conf import myMongo

# 获取Mongo的client
mongo_client = myMongo.getMyMongo()

# 切换到test数据库,如果没有就自动创建
test_db = mongo_client.test

# 使用product集合，如果没有就自动创建
coll_product = test_db.product


# save
def save(product):
    # 向集合中插入数据
    coll_product.insert({'name': product.name, 'myprice': product.myprice, 'id': product.id})


# find
def get_by_id(product_id):
    return coll_product.find_one({"id": product_id})


def get_by_name(name):
    return coll_product.find_one({'name': name})


def find_by_type(product_type):
    return coll_product.find({"type": product_type})


# 更新产品
def update_product(product):
    coll_product.save(product)
