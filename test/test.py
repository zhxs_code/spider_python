# -*- coding: UTF-8 -*-

class test():
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = []
        self.z.append(z)

    def addToZ(self, z):
        self.z.append(z)


if __name__ == "__main__":
    # 第一种测试，test类没有Init方法,属性都定义在类中，而非self的属性
    # 结论：对类实例对象中list类型的属性进行从操作会映射到类定义的结构中，说明list是引用类型


    # 第二种测试，test类有Init方法,属性都定义在类中，而非self的属性
    # 结论：无论有没有Init方法，如果属性是设置在类中，那么引用类型的数据的修改会一直保存在所有类对象中

    # 第二种测试，test类有Init方法,属性都定义为self的属性
    # 结论： 每个类实例对自己属性的操作都不会影响其他类实例对象

    t = test(1, 2, 'a')
    u = test(2, 3, 'b')
    u.z.append('c')
    print(u)
