# -*- coding: UTF-8 -*-

# 价格变动历史


class history():

    def __init__(self):
        # 商品id
        self.product_id = None

        # 商品名称
        self.name = None

        # 天猫价格变动记录
        self.tm_price_record = []

        # 狗东价格变动记录
        self.jd_price_record = []
