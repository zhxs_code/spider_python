# -*- coding: UTF-8 -*-

# 时间工具
import time


# 产品类
class product:

    def __init__(self):

        # 产品id
        self.id = ''

        # 产品名称
        self.name=''

        # 商品code
        self.code=''

        # 卖家
        self.seller=None

        # 生产商
        self.producer=None

        # 店铺
        self.shop_web_id=''

        # 基本属性
        self.basic=None

        # 价格集合:天猫和京东价格的集合
        self.myprice={}